package com.mitocode.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationCase;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.itextpdf.text.log.SysoCounter;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;
import com.mitocode.util.MensajeManager;

@Named
@ViewScoped
public class UsuariosBean implements Serializable {

	@Inject
	private IUsuarioService usuarioService;
	@Inject
	private MensajeManager mensajeManager;
	private List<Usuario> lista;
	private String tipoDialog;
	private Usuario usuario;
	private static int val = 0;

	public static int getval() {
		return val;
	}

	private Usuario us;
	private String VUsuario;
	private boolean activo;
	private String password1;
	private String password2;

	@PostConstruct
	public void init() {
		this.usuario = new Usuario();
		this.lista = new ArrayList<>();
		this.listar();
		this.activo = false;
		this.us = new Usuario();
		FacesContext context = FacesContext.getCurrentInstance();
		this.us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");

	}

	public void listar() {
		try {
			this.lista = this.usuarioService.listar();
		} catch (Exception e) {

		}
	}

	public boolean verificarPassword(Usuario u) {

		if (u.getUsuario() != null) {
			val = 0;

			try {
				u.setContrasena(getPassword1());
				usuario = this.usuarioService.login(u);

				if (val == 1) {
					activo = true;
				} else {
					activo = false;
					mensajeManager.mostrarMensaje("AVISO", "Contraseņa incorrecta", "ERROR");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return activo;

	}

	public void mostrarUsuario(Usuario u) {
		this.usuario = u;
		this.tipoDialog = "Modificar";
	}

	public void buscarUsuario() {
		String nombre;
		nombre = this.VUsuario;
		try {
			this.lista = this.usuarioService.listarU(nombre);
		} catch (Exception e) {

		}
	}

	public void operar() {
		try {
				
				String clave = getPassword2();
				String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
				this.usuario.setContrasena(claveHash);
				this.usuarioService.modificar(this.usuario);
				mensajeManager.mostrarMensaje("AVISO", "Contraseņa modificada", "INFO");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		String url = "usuarios.xhtml";
				FacesContext fc = FacesContext.getCurrentInstance();
				ExternalContext ec = fc.getExternalContext();
				try {
				        ec.redirect(url);
				} catch (IOException ex) {
				        Logger.getLogger(NavigationCase.class.getName()).log(Level.SEVERE, null, ex);
				}
		
	}

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	public String getTipoDialog() {
		return tipoDialog;
	}

	public void setTipoDialog(String tipoDialog) {
		this.tipoDialog = tipoDialog;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getVUsuario() {
		return VUsuario;
	}

	public void setVUsuario(String vUsuario) {
		VUsuario = vUsuario;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public Usuario getUs() {
		return us;
	}

	public void setUs(Usuario us) {
		this.us = us;
	}

	public static int getVal() {
		return val;
	}

	public static void setVal(int val) {
		UsuariosBean.val = val;
	}

}
